from django.db import models

# Create your models here.
class ContactDetail(models.Model):
    location = models.CharField(max_length=256, null=True,blank=True)
    number = models.IntegerField()
    email =  models.EmailField(null=True,blank=True)
    opening_date =  models.CharField(max_length=256, null=True,blank=True)
    description = models.TextField(null=True,blank=True)

class ContactUs(models.Model):
    name = models.CharField(max_length=256, null=True,blank=True)
    number = models.IntegerField()
    email =  models.EmailField(null=True,blank=True)
    message_subject = models.CharField(max_length=256, null=True,blank=True)
    messages = models.TextField(null=True,blank=True)


class Map(models.Model):
    map_link = models.URLField()