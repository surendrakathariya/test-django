# Generated by Django 4.2.1 on 2023-05-28 17:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0003_alter_aboutus_body_alter_aboutus_body_en_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutus',
            name='body',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='aboutus',
            name='body_en',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='aboutus',
            name='body_fr',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='aboutus',
            name='body_ne',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
