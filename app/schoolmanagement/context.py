

from app.about.models import SocialNetwork
from app.contact.models import ContactDetail
from app.schoolmanagement.models import Logo, Programs


def common(request):
    sociallist = SocialNetwork.objects.all()
    logolists = Logo.objects.order_by('-id')[:1]
    service =Programs.objects.order_by('-id')[:5]
    contactlists = ContactDetail.objects.order_by('-id')[:1]

    context={
        'sociallist':sociallist,
        'logolists':logolists,
        'service':service,
        'contactlists':contactlists,

    }
    return context