from django.db import models
from ckeditor.fields import RichTextField
from django.utils.text import slugify

from django.db.models.signals import pre_save
from django.dispatch import receiver
class BaseModel(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)


class Logo(BaseModel):
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)

@receiver(pre_save, sender=Logo)
def restrict_logo_entry(sender, instance, **kwargs):
    if instance.pk is None and Logo.objects.exists():
        raise ValueError("Only one entry allowed for Logo model.")

    if instance.pk is not None:
        if Logo.objects.exclude(pk=instance.pk).exists():
            raise ValueError("Only one entry allowed for Logo model.")
    instance.save()


class Event(BaseModel):
    slug = models.SlugField(max_length=256, unique=True, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    sub_title = models.CharField(max_length=100, null=True, blank=True)
    adddate = models.DateTimeField(auto_now_add=True)
    expired_date = models.DateTimeField()
    location = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    organizer_name = models.CharField(max_length=100, null=True, blank=True)
    organizer_email = models.EmailField(max_length=256, null=True, blank=True)
    organizer_phone = models.IntegerField(null=True, blank=True)

    def __str__(self) -> str:
        return self.title


class Carousel(BaseModel):
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)

    def __str__(self):
        return self.title

class ImagePopUp(BaseModel):
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)

    def __str__(self):
        return self.title
class Newletter(models.Model):
    email = models.EmailField()


class Facilities(BaseModel):
    slug = models.SlugField(max_length=256, unique=True, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)
    detailmage = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)
    body = RichTextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Facilities, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Programs(BaseModel):
    slug = models.SlugField(max_length=256, unique=True, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)
    body = RichTextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Programs, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Notice(BaseModel):
    slug = models.SlugField(max_length=256, unique=True, null=True, blank=True)
    title = models.CharField(max_length=100, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='uploads/% Y/% m/% d/', null=True, blank=True)
    start_date = models.DateField(auto_now=True)
    expire_date = models.DateField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Notice, self).save(*args, **kwargs)

    def __str__(self):
        return self.title
