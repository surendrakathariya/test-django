document.addEventListener('DOMContentLoaded', function() {
    var popup = document.getElementById('popup');
    var closeButton = popup.querySelector('.close-button');
    popup.style.display = 'block';
    closeButton.addEventListener('click', function() {
      popup.style.display = 'none';
    });
  });